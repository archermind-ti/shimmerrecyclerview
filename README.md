# shimmerrecyclerview

## 项目介绍

一个闪烁的九宫格和列表

##  安装教程

方式一：

1. 下载模块代码添加到自己的工程

2. 关联使用

   ```
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
       implementation project(':shimmer_library')
       testImplementation 'junit:junit:4.13'
   	……
   }
   ```

3. gradle sync

方式二：

```
allprojects {
    repositories {
        mavenCentral()        
    }
}

...
dependencies {
	...
	implementation 'com.gitee.archermind-ti:ShimmerRecyclerView:1.0.0-beta'
	...
}
```

## 使用说明

```java
Button button = (Button) findComponentById(ResourceTable.Id_button1);
Button button2 = (Button) findComponentById(ResourceTable.Id_button2);
Button button3 = (Button) findComponentById(ResourceTable.Id_button3);
Button button4 = (Button) findComponentById(ResourceTable.Id_button4);
button.setClickedListener(new Component.ClickedListener() {
    @Override
    public void onClick(Component component) {
        Intent intent1 = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName("com.cooltechworks.views.shimmer_library.ListTest")
                .build();
        intent1.setOperation(operation);
        startAbility(intent1);
    }
});

button3.setClickedListener(new Component.ClickedListener() {
    @Override
    public void onClick(Component component) {
        Intent intent1 = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName("com.cooltechworks.views.shimmer_library.ListTest2")
                .build();
        intent1.setOperation(operation);
        startAbility(intent1);
    }
});

button2.setClickedListener(new Component.ClickedListener() {
    @Override
    public void onClick(Component component) {
        Intent intent1 = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName("com.cooltechworks.views.shimmer_library.GridTest")
                .build();
        intent1.setOperation(operation);
        startAbility(intent1);
    }
});
button4.setClickedListener(new Component.ClickedListener() {
    @Override
    public void onClick(Component component) {
        Intent intent1 = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName("com.cooltechworks.views.shimmer_library.GridTest2")
                .build();
        intent1.setOperation(operation);
        startAbility(intent1);
    }
});
```

## 效果展示

![](1.gif)
![](2.gif)

## 版本迭代

- v1.0.0



## License

The repo is released under following licenses

[Apache License](https://github.com/sharish/ShimmerRecyclerView/blob/master/LICENSE.md) for ShimmerRecycler

[Apache License](https://github.com/team-supercharge/ShimmerLayout/blob/master/LICENSE.md) for ShimmerLayout

