package com.cooltechworks.views.shimmerrecyclerview.slice;

import com.cooltechworks.views.shimmerrecyclerview.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Button button = (Button) findComponentById(ResourceTable.Id_button1);
        Button button2 = (Button) findComponentById(ResourceTable.Id_button2);
        Button button3 = (Button) findComponentById(ResourceTable.Id_button3);
        Button button4 = (Button) findComponentById(ResourceTable.Id_button4);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent1 = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getBundleName())
                        .withAbilityName("com.cooltechworks.views.shimmer_library.ListTest")
                        .build();
                intent1.setOperation(operation);
                startAbility(intent1);
            }
        });

        button3.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent1 = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getBundleName())
                        .withAbilityName("com.cooltechworks.views.shimmer_library.ListTest2")
                        .build();
                intent1.setOperation(operation);
                startAbility(intent1);
            }
        });

        button2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent1 = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getBundleName())
                        .withAbilityName("com.cooltechworks.views.shimmer_library.GridTest")
                        .build();
                intent1.setOperation(operation);
                startAbility(intent1);
            }
        });
        button4.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent1 = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getBundleName())
                        .withAbilityName("com.cooltechworks.views.shimmer_library.GridTest2")
                        .build();
                intent1.setOperation(operation);
                startAbility(intent1);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
