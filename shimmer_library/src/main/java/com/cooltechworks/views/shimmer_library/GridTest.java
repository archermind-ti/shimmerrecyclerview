package com.cooltechworks.views.shimmer_library;

import com.cooltechworks.views.shimmer_library.slice.GridTestSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class GridTest extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(GridTestSlice.class.getName());
    }
}
