package com.cooltechworks.views.shimmer_library;

import com.cooltechworks.views.shimmer_library.slice.GridTest2Slice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class GridTest2 extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(GridTest2Slice.class.getName());
    }
}
