package com.cooltechworks.views.shimmer_library;

import com.cooltechworks.views.shimmer_library.slice.ListTestSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ListTest extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ListTestSlice.class.getName());
    }
}
