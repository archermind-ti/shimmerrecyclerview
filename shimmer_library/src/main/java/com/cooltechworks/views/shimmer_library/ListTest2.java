package com.cooltechworks.views.shimmer_library;

import com.cooltechworks.views.shimmer_library.slice.ListTest2Slice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ListTest2 extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ListTest2Slice.class.getName());
    }
}
