package com.cooltechworks.views.shimmer_library;

public class SettingItem {

    private int imageId;
    private String settingName;
    private String settingName2;
    private String settingName3;

    public SettingItem(int imageId, String settingName, String settingName2, String settingName3) {
        this.imageId = imageId;
        this.settingName = settingName;
        this.settingName2 = settingName2;
        this.settingName3 = settingName3;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getSettingName2() {
        return settingName2;
    }

    public void setSettingName2(String settingName2) {
        this.settingName2 = settingName2;
    }

    public String getSettingName3() {
        return settingName3;
    }

    public void setSettingName3(String settingName3) {
        this.settingName3 = settingName3;
    }

    public String getSettingName() {
        return settingName;
    }

    public void setSettingName(String settingName) {
        this.settingName = settingName;
    }
}
