package com.cooltechworks.views.shimmer_library;

import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.agp.utils.Color;

import java.util.List;

public class SettingProvider2 extends BaseItemProvider {
    private List<SettingItem> settingList;
    private AbilitySlice slice;
    Color textColor;
    private boolean running = false;
    private float transX = 0;
    private AnimatorValue anim;

    public SettingProvider2(List<SettingItem> list, AbilitySlice slice) {
        this.settingList = list;
        this.slice = slice;
    }

    public class SettingHolder {
        Image settingIma;
        Text settingText;
        Text settingText2;
        Text settingText3;
        Component cptDefault;
        Component cpt;
        Component content;

        SettingHolder(Component component) {
            settingIma = (Image) component.findComponentById(ResourceTable.Id_ima_setting);
            settingText = (Text) component.findComponentById(ResourceTable.Id_text_setting);
            settingText2 = (Text) component.findComponentById(ResourceTable.Id_text_setting2);
            settingText3 = (Text) component.findComponentById(ResourceTable.Id_text_setting3);
            cptDefault = component.findComponentById(ResourceTable.Id_cptDefault);
            cpt = component.findComponentById(ResourceTable.Id_component);
            content = component.findComponentById(ResourceTable.Id_content);
        }
    }
    @Override
    public int getCount() {
        return settingList == null ? 0 : settingList.size();
    }

    @Override
    public Object getItem(int position) {
        if (settingList != null && position >= 0 && position < settingList.size()) {
            return settingList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        SettingHolder holder;
        SettingItem setting = settingList.get(position);
        if (component == null) {
            component = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_list_item_view2, null, false);
            holder = new SettingHolder(component);
            // 将获取到的子组件信息绑定到列表项的实例中
            component.setTag(holder);
        } else {
            // 从缓存中获取到列表项实例后，直接使用绑定的子组件信息进行数据填充。
            holder = (SettingHolder) component.getTag();
        }
        holder.settingIma.setPixelMap(setting.getImageId());
        holder.settingText.setText(setting.getSettingName());
        holder.settingText2.setText(setting.getSettingName2());
        holder.settingText3.setText(setting.getSettingName3());
        holder.cptDefault.setVisibility(running ? Component.VISIBLE : Component.HIDE);
        holder.cpt.setVisibility(running ? Component.VISIBLE : Component.HIDE);
        holder.cpt.setTranslationX(transX);
        holder.content.setVisibility(running ? Component.HIDE : Component.VISIBLE);

        if (textColor != null) {
            holder.settingText.setTextColor(textColor);
        }
        return component;
    }

    public void startAnim() {
        if (running) {
            return;
        }

        anim = new AnimatorValue();
        anim.setDuration(1000);
        anim.setCurveType(Animator.CurveType.LINEAR);
        anim.setLoopedCount(1);
        anim.setDelay(200);
        anim.setValueUpdateListener((animatorValue, v) -> {
            System.out.println("======v = " + v);
            transX = v * 1000;
            notifyDataChanged();
        });
        anim.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                System.out.println("======onStart");
                running = true;
            }

            @Override
            public void onStop(Animator animator) {
                System.out.println("======onStop");
                running = false;
                notifyDataChanged();
            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                System.out.println("======onEnd");
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        anim.start();
    }

}
