package com.cooltechworks.views.shimmer_library;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class TestGridItemProvider2 extends BaseItemProvider {

    /**
     * 数据条目
     */
    private List<SettingItem> mData;
    /**
     * 每一行的个数
     */
    private final int LINE_COUNT = 2;
    private Context mContext;
    private boolean running = false;
    private AnimatorValue anim;
    private float transX = 0;

    public TestGridItemProvider2(Context context) {
        mContext = context;
        mData = new ArrayList<>(100);
        //创建数据
        mData.add(new SettingItem(ResourceTable.Media_SamsungOn7, "Samsung On7", "$1,200 Off", "Now $8990"));
        mData.add(new SettingItem(ResourceTable.Media_Lenovo, "Lenovo Vibe K5 Note", "Exchange Offer", "From $11,999"));
        mData.add(new SettingItem(ResourceTable.Media_googlePixel, "Google Pixel | XL", "Exchange Offer", "From $57,000"));
        mData.add(new SettingItem(ResourceTable.Media_appleIphone6, "Apple iPhone6", "$3,000 Off", "Now $33990"));
        mData.add(new SettingItem(ResourceTable.Media_samsungGalaxy, "Samsung Galaxy S7", "Just $;43,000", "4 GB RAM, 32 GB ROM"));
        mData.add(new SettingItem(ResourceTable.Media_motorolaMoto, "Moto Turbo", "Exchange Offer", "Now $31990"));

    }
    @Override
    public int getCount() {
        //计算总共要多少行
        int needLines = mData.size() / LINE_COUNT;
        if (mData.size() % LINE_COUNT > 0) {
            needLines++;
        }
        return needLines;
    }

    @Override
    public List<SettingItem> getItem(int i) {
        int startIndex = i * LINE_COUNT;
        int endIndex = (i + 1) * LINE_COUNT;
        if (endIndex > mData.size()) {
            endIndex = mData.size();
        }
        //此处返回一行的数据列表
        return mData.subList(startIndex, endIndex);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        ViewHolder holder;
        if (component == null) {
            holder = new ViewHolder();
            component = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_grid_list_item2, null, false);
            holder.cptDefault1 = component.findComponentById(ResourceTable.Id_cptDefault1);
            holder.cptDefault2 = component.findComponentById(ResourceTable.Id_cptDefault2);
            holder.cpt1 = holder.cptDefault1.findComponentById(ResourceTable.Id_component);
            holder.cpt2 = holder.cptDefault2.findComponentById(ResourceTable.Id_component);
            holder.contentLeft = component.findComponentById(ResourceTable.Id_contentLeft);
            holder.contentRight = component.findComponentById(ResourceTable.Id_contentRight);
            holder.image1 = (Image) component.findComponentById(ResourceTable.Id_image1);
            holder.image2 = (Image) component.findComponentById(ResourceTable.Id_image2);
            holder.text1 = (Text) component.findComponentById(ResourceTable.Id_text1);
            holder.text2 = (Text) component.findComponentById(ResourceTable.Id_text2);
            holder.text3 = (Text) component.findComponentById(ResourceTable.Id_text3);
            holder.text4 = (Text) component.findComponentById(ResourceTable.Id_text4);
            holder.text5 = (Text) component.findComponentById(ResourceTable.Id_text5);
            holder.text6 = (Text) component.findComponentById(ResourceTable.Id_text6);
            component.setTag(holder);
        } else {
            holder = (ViewHolder) component.getTag();
        }
        //获取一行的数据
        List<SettingItem> settingItemList = getItem(i);
        //添加每一行的控件,添加数据
        holder.image1.setPixelMap(settingItemList.get(0).getImageId());
        holder.image2.setPixelMap(settingItemList.get(1).getImageId());
        holder.text1.setText(settingItemList.get(0).getSettingName());
        holder.text2.setText(settingItemList.get(0).getSettingName2());
        holder.text3.setText(settingItemList.get(0).getSettingName3());
        holder.text4.setText(settingItemList.get(1).getSettingName());
        holder.text5.setText(settingItemList.get(1).getSettingName2());
        holder.text6.setText(settingItemList.get(1).getSettingName3());

        holder.cptDefault1.setVisibility(running ? Component.VISIBLE : Component.HIDE);
        holder.cptDefault2.setVisibility(running ? Component.VISIBLE : Component.HIDE);
        holder.cpt1.setVisibility(running ? Component.VISIBLE : Component.HIDE);
        holder.cpt2.setVisibility(running ? Component.VISIBLE : Component.HIDE);
        holder.cpt1.setTranslationX(transX);
        holder.cpt2.setTranslationX(transX);
        holder.contentLeft.setVisibility(running ? Component.HIDE : Component.VISIBLE);
        holder.contentRight.setVisibility(running ? Component.HIDE : Component.VISIBLE);
        return component;
    }

    static class ViewHolder {
        private Component cpt1;
        private Component cpt2;
        private Component cptDefault1;
        private Component cptDefault2;
        private Component contentLeft;
        private Component contentRight;
        private Image image1;
        private Image image2;
        private Text text1;
        private Text text2;
        private Text text3;
        private Text text4;
        private Text text5;
        private Text text6;

    }
    public void startAnim() {
        if (running) {
            return;
        }

        anim = new AnimatorValue();
        anim.setDuration(1000);
        anim.setCurveType(Animator.CurveType.LINEAR);
        anim.setLoopedCount(1);
        anim.setDelay(200);
        anim.setValueUpdateListener((animatorValue, v) -> {
            System.out.println("======v = " + v);
            transX = v * 1000;
            notifyDataChanged();
        });
        anim.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                System.out.println("======onStart");
                running = true;
            }

            @Override
            public void onStop(Animator animator) {
                System.out.println("======onStop");
                running = false;
                notifyDataChanged();
            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                System.out.println("======onEnd");
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        anim.start();
    }
}
