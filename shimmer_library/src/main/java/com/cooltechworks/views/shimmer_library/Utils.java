package com.cooltechworks.views.shimmer_library;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

public class Utils {
    public static int getScreenWidth(Context context) {
        DisplayManager displayManager = DisplayManager.getInstance();
        Display display = displayManager.getDefaultDisplay(context).get();
        DisplayAttributes displayAttributes = display.getAttributes();
        int width = displayAttributes.width;
        return width;
    }
}
