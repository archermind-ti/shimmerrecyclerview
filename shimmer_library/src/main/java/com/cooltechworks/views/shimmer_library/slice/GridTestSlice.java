package com.cooltechworks.views.shimmer_library.slice;

import com.cooltechworks.views.shimmer_library.ResourceTable;
import com.cooltechworks.views.shimmer_library.TestGridItemProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;

public class GridTestSlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_grid_test);
        initView();
    }


    private void initView() {
        ListContainer listGrid = (ListContainer) findComponentById(ResourceTable.Id_list_grid);
        TestGridItemProvider provider = new TestGridItemProvider(this);
        listGrid.setItemProvider(provider);
        provider.startAnim();
    }


}