package com.cooltechworks.views.shimmer_library.slice;

import com.cooltechworks.views.shimmer_library.ResourceTable;
import com.cooltechworks.views.shimmer_library.SettingItem;
import com.cooltechworks.views.shimmer_library.SettingProvider;
import com.cooltechworks.views.shimmer_library.SettingProvider2;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.app.dispatcher.TaskDispatcher;

import java.util.ArrayList;
import java.util.List;

public class ListTest2Slice extends AbilitySlice {

    private ListContainer listContainer;
    TaskDispatcher taskDispatcher;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_recycler_view);
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);
    }


    private List<SettingItem> getData() {
        ArrayList<SettingItem> data = new ArrayList<>();
        data.add(new SettingItem(
                ResourceTable.Media_agni2, "Agni 5, India\'s Longest Range Nuclear Capable Missile, Successfully Test Fired: 10 Points",
                "Agni-5, India\'s longest range nuclear capable missile, was successfully test fired from the Kalam Island off Odisha coast today by the Defence Research and Development Organisation or DRDO. The intercontinental surface-to-surface ballistic missile, the latest in India\\'s \\\"Agni\\\" family of medium to intercontinental range missiles, with new technology for navigation and guidance, gives India the strategic depth it needs to contain its enemies, say scientists. Ready to be deployed, the Agni-5 will soon join India\\'s military arsenal",
                "You\'ve shown interest in Nuclear Capable Missles"
        ));
        data.add(new SettingItem(
                ResourceTable.Media_phone, "OnePlus releases leather case for OnePlus 3 and 3T",
                "OnePlus has released a leather case for the OnePlus 3/3T. Even though there are several cases for the OnePlus 3/3T, this one will offer a premium look for the smartphone. It has textured calfskin leather blend along with a classic matte finish making it easy to clean and also offers a comfortable grip.",
                "You\'ve shown interest in One Plus"
        ));
        data.add(new SettingItem(
                0, "Who Will Ride The Third Dragon In \'Game Of Thrones\' Season 7?",
                "Game of Thrones season 7 will kick off a new age of Game of Thrones, with the show becoming a true epic fantasy for the first time. Don’t worry, it will remain a high political drama too, but the war between ice and fire isn’t far off now, and Daenerys Targaryen seems destined to lead the fight",
                "You\'ve shown interest in Game of Thrones series"
        ));
        data.add(new SettingItem(
                ResourceTable.Media_jetairways, "Jet Airways Extends Sale, Offers Tickets Starting Rs. 990",
                "Jet Airways has extended its promotional scheme offering fares starting as low as Rs. 990 on select domestic flights under economy class travel. Under Jet Airways\\' \\\"Best Fares Forever\\\" offer, tickets must be purchased till December 27, 2016, for travel on or after January 4, 2017. \\\"Tickets must be purchased a minimum of 15 days prior departure,\\\" the airline said on its website, adding that limited seats are available under the offer on a first come, first serve basis.",
                "You\'ve shown interest in Jet Airways"
        ));
        return data;
    }

    @Override
    public void onActive() {
        super.onActive();
        SettingProvider2 provider = new SettingProvider2(getData(), this);
        listContainer.setItemProvider(provider);
        provider.startAnim();

    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
